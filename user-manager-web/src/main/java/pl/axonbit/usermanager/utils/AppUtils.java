package pl.axonbit.usermanager.utils;

import javax.servlet.http.HttpSession;

import pl.axonbit.usermanager.domain.model.user.User;

public class AppUtils {

    public static void storeLoginedUser(HttpSession session, User loginedUser) {
        session.setAttribute("loginedUser", loginedUser);
    }

    public static void removeLoginedUser(HttpSession session) {
        session.removeAttribute("loginedUser");
    }

    public static User getLoginedUser(HttpSession session) {
        User loginedUser = (User) session.getAttribute("loginedUser");
        return loginedUser;
    }

}
