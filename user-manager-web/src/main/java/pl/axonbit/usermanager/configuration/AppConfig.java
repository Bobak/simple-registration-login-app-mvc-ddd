package pl.axonbit.usermanager.configuration;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.mvc.jsp.JspMvcFeature;
import org.reflections.Reflections;

public class AppConfig extends ResourceConfig {

    private static final Reflections reflections = new Reflections("pl.axonbit.usermanager");

    public AppConfig() {
        packages("pl.axonbit.usermanager.controlers");
        property(JspMvcFeature.TEMPLATE_BASE_PATH, "/WEB-INF/jsp");
        register(JspMvcFeature.class);
        try {
            new ConfigurationHelper(this, reflections, "local");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
