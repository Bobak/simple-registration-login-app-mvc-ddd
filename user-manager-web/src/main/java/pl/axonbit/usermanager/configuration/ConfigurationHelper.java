package pl.axonbit.usermanager.configuration;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.EventListener;
import java.util.logging.Logger;

import javax.inject.Singleton;

import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.reflections.Reflections;

import pl.axonbit.usermanager.domain.model.Service;
import pl.axonbit.usermanager.port.adapter.persistance.sql.MySQLConfig;
import pl.axonbit.usermanager.port.adapter.persistance.sql.MySQLRepository;

public class ConfigurationHelper {

    private static final Logger LOGGER = Logger.getLogger(ConfigurationHelper.class.getName());

    public ConfigurationHelper(AppConfig appConfig, Reflections reflections, String envName) throws IOException {

        LOGGER.info("Environment: " + envName);
        configMySQLConnction(appConfig, envName);
        configMySQLRepositories(appConfig, reflections);
        configServices(appConfig, reflections);
        configInMemoryEventListeners(appConfig, reflections);

    }

    private void configMySQLConnction(AppConfig appConfig, String envName) throws IOException {

        var config = MySQLConfig.createConfig(envName);

        try {
            Class.forName("com.mysql.jdbc.Driver");
            final var connection = DriverManager.getConnection(config.getUrl(), config.getUser(), config.getPassword());

            AbstractBinder binder = new AbstractBinder() {

                @Override
                protected void configure() {
                    bind(connection).to(Connection.class);
                }
            };

            appConfig.register(binder);

        } catch (Exception e) {
            e.printStackTrace();
        }
        LOGGER.info("MysQL connection with url" + config.getUrl() + " has been configured");
    }

    private void configMySQLRepositories(AppConfig appConfig, Reflections reflections) {

        LOGGER.info("Repositories for MySQL configuration: ");

        appConfig.register(new AbstractBinder() {

            @Override
            protected void configure() {

                for (Class<? extends MySQLRepository> resource : reflections.getSubTypesOf(MySQLRepository.class)) {

                    bind(resource).to(resource.getInterfaces()[0]).in(Singleton.class);

                    LOGGER.info("Repository: " + resource.getSimpleName() + " has been configured");
                }
            }
        });
    }

    private void configServices(AppConfig appConfig, Reflections reflections) {

        LOGGER.info("Services configuration: ");

        appConfig.register(new AbstractBinder() {

            @Override
            protected void configure() {

                for (Class<? extends Service> resource : reflections.getSubTypesOf(Service.class)) {

                    if (!resource.isInterface()) {
                        bindAsContract(resource);
                        LOGGER.info("Service: " + resource.getSimpleName() + " has been configured");
                    } else {
                        for (Class interfacesImpl : reflections.getSubTypesOf(resource)) {
                            bind(interfacesImpl).to(resource);
                            LOGGER.info("Service: " + " impl: " + interfacesImpl.getSimpleName() + "/"
                                    + resource.getSimpleName() + " has been configured");
                        }

                    }

                }
            }
        });
    }

    private void configInMemoryEventListeners(AppConfig appConfig, Reflections reflections) {

        LOGGER.info("InMemory listener configuration ");

        appConfig.register(new AbstractBinder() {
            @Override
            protected void configure() {
                for (Class resource : reflections.getSubTypesOf(EventListener.class)) {
                    bindAsContract(resource);
                    LOGGER.info("Event listener : " + resource.getSimpleName() + " has been configured");
                }
            }
        });
    }

}
