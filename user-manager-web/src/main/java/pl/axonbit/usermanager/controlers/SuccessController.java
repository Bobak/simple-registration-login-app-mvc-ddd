package pl.axonbit.usermanager.controlers;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.mvc.Viewable;

@Path("/success")
public class SuccessController {

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Viewable success() {
        return new Viewable("/success");
    }
}
