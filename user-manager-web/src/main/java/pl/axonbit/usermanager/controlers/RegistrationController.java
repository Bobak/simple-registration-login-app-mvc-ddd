package pl.axonbit.usermanager.controlers;

import java.util.HashMap;

import javax.inject.Inject;
import javax.validation.constraints.NotEmpty;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.mvc.Viewable;

import pl.axonbit.usermanager.application.EventAppService;
import pl.axonbit.usermanager.application.user.RegisterUserCommand;
import pl.axonbit.usermanager.application.user.UserApplicationService;
import pl.axonbit.usermanager.port.adapter.messaging.InMemoryUserListener;

@Path("/registry")
public class RegistrationController {

    @Inject
    UserApplicationService userApplicationService;
    @Inject
    InMemoryUserListener inMemoryUserListener;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Viewable registry() {
        var model = new HashMap<String, String>();
        return new Viewable("/registry", model);
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public Viewable submit(@NotEmpty @FormParam("login") final String login,
            @NotEmpty @FormParam("email") final String email, @NotEmpty @FormParam("password") final String password) {

        new EventAppService();
        inMemoryUserListener.listening();
        try {
            userApplicationService.registerNewUser(new RegisterUserCommand(password, login, email));
            return new Viewable("/success");

        } catch (Exception e) {
            var model = new HashMap<>();
            model.put("errorMessage", e.getMessage());

            return new Viewable("/registry", model);
        }

    }

}
