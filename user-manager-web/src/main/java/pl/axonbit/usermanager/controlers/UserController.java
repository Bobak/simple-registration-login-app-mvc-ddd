package pl.axonbit.usermanager.controlers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.mvc.Viewable;

import pl.axonbit.usermanager.utils.AppUtils;

@Path("/user")
public class UserController {

    @Context
    private HttpServletRequest httpRequest;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Viewable user() {

        var user = AppUtils.getLoginedUser(httpRequest.getSession());
        if (user == null) {
            return new Viewable("/error");
        }
        return new Viewable("/user");
    }

}
