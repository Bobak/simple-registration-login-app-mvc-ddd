package pl.axonbit.usermanager.controlers;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotEmpty;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.mvc.Viewable;

import pl.axonbit.usermanager.domain.model.user.UserRepository;
import pl.axonbit.usermanager.port.adapter.services.Sha512HashPasswordService;
import pl.axonbit.usermanager.utils.AppUtils;

@Path("/login")
public class LoginController {

    @Inject
    UserRepository repository;
    @Inject
    Sha512HashPasswordService sha512HashPasswordService;
    @Context
    private HttpServletRequest httpRequest;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Viewable login() {
        return new Viewable("/login");
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.TEXT_HTML)
    public Viewable submit(@NotEmpty @FormParam("login") final String login,
            @NotEmpty @FormParam("password") final String password) {

        final var encryptedPassword = sha512HashPasswordService.hashPassword(password);
        final var user = repository.getByLoginPasswordEncryped(login, encryptedPassword);

        
        if (user != null) {
            AppUtils.storeLoginedUser(httpRequest.getSession(), user);

            var model = new HashMap<String, String>();
            model.put("login", user.getLogin());
            model.put("email", user.getEmail());
            return new Viewable("/user", model);
        }
        var model = new HashMap<>();
        model.put("errorMessage", "Login or Password not match !!");

        return new Viewable("/login", model);

    }

}
