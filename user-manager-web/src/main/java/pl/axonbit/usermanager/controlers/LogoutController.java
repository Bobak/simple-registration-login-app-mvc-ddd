package pl.axonbit.usermanager.controlers;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.mvc.Viewable;

import pl.axonbit.usermanager.utils.AppUtils;

@Path("/logout")
public class LogoutController {

    @Context
    private HttpServletRequest httpRequest;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Viewable logout() {
        AppUtils.removeLoginedUser(httpRequest.getSession());
        return new Viewable("/index");
    }

}
