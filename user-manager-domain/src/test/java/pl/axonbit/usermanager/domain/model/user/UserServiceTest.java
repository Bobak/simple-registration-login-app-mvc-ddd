package pl.axonbit.usermanager.domain.model.user;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class UserServiceTest {

    private final UserService userService;

    public UserServiceTest() {
        this.userService = new UserService();
    }

    @Test
    public void loginValidation_text_only() {
        assertThrows(IllegalStateException.class, () -> {
            userService.validateLogin("TXT");
        });
    }

    @Test
    public void loginValidation_text_plus_numbers() {
        assertThrows(IllegalStateException.class, () -> {
            userService.validateLogin("TXT12");
        });
    }

    @Test
    public void loginValidation_numbers_only_too_small() {
        assertThrows(IllegalStateException.class, () -> {
            userService.validateLogin("1");
        });
    }

    @Test
    public void loginValidation_numbers_only_too_many() {
        assertThrows(IllegalStateException.class, () -> {
            userService.validateLogin("12345678");
        });
    }

    @Test
    public void loginValidation_numbers_only_ok() {
        assertDoesNotThrow(() -> {
            userService.validateLogin("1234567");
        });
    }

    @Test
    public void passwordValidation_no_ok() {
        assertThrows(IllegalStateException.class, () -> {
            userService.validatePassword("password");
        });
    }

    @Test
    public void passwordValidation_ok() {
        assertDoesNotThrow(() -> {
            userService.validatePassword("passpasspass");
        });
    }

}
