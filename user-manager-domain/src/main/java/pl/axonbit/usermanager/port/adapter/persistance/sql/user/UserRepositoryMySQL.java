package pl.axonbit.usermanager.port.adapter.persistance.sql.user;

import java.sql.Connection;
import java.sql.Statement;
import java.util.UUID;

import javax.inject.Inject;

import pl.axonbit.usermanager.domain.model.user.User;
import pl.axonbit.usermanager.domain.model.user.UserId;
import pl.axonbit.usermanager.domain.model.user.UserRepository;
import pl.axonbit.usermanager.port.adapter.persistance.sql.MySQLRepository;

public class UserRepositoryMySQL extends MySQLRepository implements UserRepository {

    private static final String TABLE_NAME = "users";
    private static final String DB_NAME = "user";
    private static final String CREATE_USRER_DATABASE_IF_NOT_EXISTS = "CREATE DATABASE IF NOT EXISTS " + DB_NAME + ";";
    private static final String CREATE_USRER_TABLE_IF_NOT_EXISTS = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "( "
            + UserMySQLRsultSetSerializer.ID + " varchar(100) NOT NULL PRIMARY KEY, "
            + UserMySQLRsultSetSerializer.PASSWORD + " varchar(150) NOT NULL, " + UserMySQLRsultSetSerializer.LOGIN
            + " varchar(7) NOT NULL, " + UserMySQLRsultSetSerializer.EMAIL + " varchar(50) NOT NULL, "
            + UserMySQLRsultSetSerializer.STATUS + " varchar(20) NOT NULL ) ENGINE=InnoDB;";
    private static final UserMySQLRsultSetSerializer userMySQLRsultSetSerializer = new UserMySQLRsultSetSerializer();
    private Connection connection;

    @Inject
    public UserRepositoryMySQL(Connection connection) {
        this.connection = connection;
        init();
    }

    public void init() {

        try {
            Statement s = connection.createStatement();
            s.executeUpdate(CREATE_USRER_DATABASE_IF_NOT_EXISTS);
            s.executeUpdate(CREATE_USRER_TABLE_IF_NOT_EXISTS);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public User get(UserId userId) {
        try {
            var query = "SELECT * FROM " + TABLE_NAME + " WHERE " + UserMySQLRsultSetSerializer.ID + "='"
                    + userId.getId() + "' LIMIT 1;";
            Statement s = connection.createStatement();
            var rs = s.executeQuery(query);
            return userMySQLRsultSetSerializer.deserialize(rs);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void insert(User user) {
        try {
            var query = "INSERT into " + TABLE_NAME + " (" + UserMySQLRsultSetSerializer.ID + ","
                    + UserMySQLRsultSetSerializer.PASSWORD + "," + UserMySQLRsultSetSerializer.LOGIN + ","
                    + UserMySQLRsultSetSerializer.EMAIL + "," + UserMySQLRsultSetSerializer.STATUS + ") VALUES ('"
                    + user.getUserId().getId() + "','" + user.getPassword() + "','" + user.getLogin() + "','"
                    + user.getEmail() + "','" + user.getUserStatus().name() + "');";
            Statement s = connection.createStatement();
            s.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public User getByLogin(String login) {
        try {
            var query = "SELECT * FROM " + TABLE_NAME + " WHERE " + UserMySQLRsultSetSerializer.LOGIN + "='" + login
                    + "' LIMIT 1;";
            Statement s = connection.createStatement();
            var rs = s.executeQuery(query);
            return userMySQLRsultSetSerializer.deserialize(rs);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User getByLoginPasswordEncryped(String login, String encryptedPassword) {
        try {
            var query = "SELECT * FROM " + TABLE_NAME + " WHERE " + UserMySQLRsultSetSerializer.LOGIN + "='" + login
                    + "' AND " + UserMySQLRsultSetSerializer.PASSWORD + "='" + encryptedPassword + "' LIMIT 1;";
            Statement s = connection.createStatement();
            var rs = s.executeQuery(query);
            return userMySQLRsultSetSerializer.deserialize(rs);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public UserId nextIdentity() {
        return new UserId(UUID.randomUUID().toString().toUpperCase());
    }

}
