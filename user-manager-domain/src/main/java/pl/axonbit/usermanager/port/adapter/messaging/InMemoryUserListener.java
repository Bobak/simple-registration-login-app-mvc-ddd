package pl.axonbit.usermanager.port.adapter.messaging;

import java.util.EventListener;

import javax.inject.Inject;

import pl.axonbit.usermanager.application.user.UserEventHandlers;
import pl.axonbit.usermanager.domain.model.DomainEventPublisher;
import pl.axonbit.usermanager.domain.model.DomainEventSubscriber;
import pl.axonbit.usermanager.domain.model.user.UserRegistered;

public class InMemoryUserListener implements EventListener {

    private final UserEventHandlers userEventHandlers;

    @Inject
    public InMemoryUserListener(UserEventHandlers userEventHandlers) {
        this.userEventHandlers = userEventHandlers;
    }

    public void listening() {
        userRegisteredHandler();
    }

    private void userRegisteredHandler() {

        DomainEventSubscriber subscriber = new DomainEventSubscriber<UserRegistered>() {
            @Override
            public void handleEvent(UserRegistered aDomainEvent) {

                userEventHandlers.when(aDomainEvent);
            }

            @Override
            public Class<UserRegistered> subscribedToEventType() {
                return UserRegistered.class;
            }
        };
        DomainEventPublisher.instance().subscribe(subscriber);
    }

}
