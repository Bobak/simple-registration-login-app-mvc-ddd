package pl.axonbit.usermanager.port.adapter.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.axonbit.usermanager.domain.model.user.Email;
import pl.axonbit.usermanager.domain.model.user.SenderService;

public class LocalFileSenderService implements SenderService {

    private static final Logger LOGGER = Logger.getLogger(LocalFileSenderService.class.getName());
    private static final String PATH = "/tmp/";

    @Override
    public void sendEmail(Email email) {
        try {
            Writer output = null;
            final var name = PATH + email.getFrom() + "_email.txt";
            File file = new File(name);
            output = new BufferedWriter(new FileWriter(file));

            output.write("EMAIL FROM:" + email.getFrom() + "\n");
            output.write("EMAIL TO:" + email.getTo() + "\n");
            output.write("EMAIL SUBJCT:" + email.getSubject() + "\n\n");
            output.write("EMAIL CONTENT:\n\n" + email.getContent() + "\n");

            output.close();
            LOGGER.log(Level.INFO, "Email has ben sent to local file " + name);

        } catch (Exception e) {
            System.out.println("Could not create file");
        }

    }

}
