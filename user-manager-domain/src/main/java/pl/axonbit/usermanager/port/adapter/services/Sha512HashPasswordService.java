package pl.axonbit.usermanager.port.adapter.services;

import org.apache.commons.codec.digest.DigestUtils;

import pl.axonbit.usermanager.domain.model.user.EncryptionPasswordService;

public class Sha512HashPasswordService implements EncryptionPasswordService {

    @Override
    public String hashPassword(String password) {
        return DigestUtils.sha512Hex(password);
    }

}
