package pl.axonbit.usermanager.port.adapter.persistance.sql;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MySQLConfig {

    private String url;
    private String user;
    private String password;

    public static MySQLConfig createConfig(String env) throws IOException {
        return createConfig(env, "config.properties");
    }

    public static MySQLConfig createConfig(String env, String fileName) throws IOException {
        return new MySQLConfig(env, fileName);
    }

    private MySQLConfig(String env, String fileName) throws IOException {
        getPropValues(env, fileName);
    }

    private void getPropValues(String env, String fileName) throws IOException {

        Properties prop = new Properties();
        String propFileName = fileName;
        InputStream inputStream;

        inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

        if (inputStream != null) {
            prop.load(inputStream);
            this.url = prop.getProperty("mysql." + env + ".url");
            this.user = prop.getProperty("mysql." + env + ".user");
            this.password = prop.getProperty("mysql." + env + ".password");
            inputStream.close();
        } else {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }
    }

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }
}
