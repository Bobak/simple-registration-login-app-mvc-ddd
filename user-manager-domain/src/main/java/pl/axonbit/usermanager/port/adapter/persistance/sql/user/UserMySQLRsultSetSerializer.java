package pl.axonbit.usermanager.port.adapter.persistance.sql.user;

import java.sql.ResultSet;

import pl.axonbit.usermanager.domain.model.Serializer;
import pl.axonbit.usermanager.domain.model.user.User;
import pl.axonbit.usermanager.domain.model.user.UserId;
import pl.axonbit.usermanager.domain.model.user.UserStatus;

public class UserMySQLRsultSetSerializer implements Serializer<ResultSet, User> {

    public static final String ID = "user_id";
    public static final String PASSWORD = "password";
    public static final String LOGIN = "login";
    public static final String EMAIL = "email";
    public static final String STATUS = "status";

    @Override
    public User deserialize(ResultSet object) {

        try {
            if (object.next()) {
                var userId = new UserId(object.getString(ID));
                var password = object.getString(PASSWORD);
                var login = object.getString(LOGIN);
                var email = object.getString(EMAIL);
                var status = UserStatus.valueOf(object.getString(STATUS));
                return new User(userId, password, login, email, status);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
