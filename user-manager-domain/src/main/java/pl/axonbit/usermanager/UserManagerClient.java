/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.axonbit.usermanager;

import java.sql.DriverManager;

import pl.axonbit.usermanager.application.EventAppService;
import pl.axonbit.usermanager.application.user.RegisterUserCommand;
import pl.axonbit.usermanager.application.user.UserApplicationService;
import pl.axonbit.usermanager.application.user.UserEventHandlers;
import pl.axonbit.usermanager.domain.model.user.UserService;
import pl.axonbit.usermanager.port.adapter.messaging.InMemoryUserListener;
import pl.axonbit.usermanager.port.adapter.persistance.sql.MySQLConfig;
import pl.axonbit.usermanager.port.adapter.persistance.sql.user.UserRepositoryMySQL;
import pl.axonbit.usermanager.port.adapter.services.LocalFileSenderService;
import pl.axonbit.usermanager.port.adapter.services.Sha512HashPasswordService;

public class UserManagerClient {

    public static void main(String[] args) throws InterruptedException {

        new EventAppService();

        var senderService = new LocalFileSenderService();
        var userEventHandlers = new UserEventHandlers(senderService);
        new InMemoryUserListener(userEventHandlers);

        try {
            var config = MySQLConfig.createConfig("local");
            final var connection = DriverManager.getConnection(config.getUrl(), config.getUser(), config.getPassword());

            final var userRepository = new UserRepositoryMySQL(connection);
            final var userService = new UserService();
            final var hashPasswordService = new Sha512HashPasswordService();
            final var userApplicationService = new UserApplicationService(userRepository, userService,
                    hashPasswordService);

            userApplicationService
                    .registerNewUser(new RegisterUserCommand("qwertyuiopas", "1234569", "statsek1@gmail.com"));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
