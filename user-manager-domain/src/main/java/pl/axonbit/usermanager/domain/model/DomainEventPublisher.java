/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.axonbit.usermanager.domain.model;

import java.util.ArrayList;
import java.util.List;

public class DomainEventPublisher {

  private static final ThreadLocal<List<DomainEventSubscriber<DomainEvent>>> subscribers =
      new ThreadLocal<List<DomainEventSubscriber<DomainEvent>>>();

  private static final ThreadLocal<Boolean> publishing =
      new ThreadLocal<Boolean>() {
        protected Boolean initialValue() {
          return Boolean.FALSE;
        }
      };

  public static DomainEventPublisher instance() {
    return new DomainEventPublisher();
  }

  public DomainEventPublisher() {
    super();
  }

  public void publish(final DomainEvent aDomainEvent) {
    if (publishing.get()) {
      return;
    }
    try {
      publishing.set(Boolean.TRUE);
      List<DomainEventSubscriber<DomainEvent>> registeredSubscribers = subscribers.get();
      if (registeredSubscribers != null) {
        Class<?> eventType = aDomainEvent.getClass();
        for (DomainEventSubscriber<DomainEvent> subscriber : registeredSubscribers) {
          Class<?> subscribedTo = subscriber.subscribedToEventType();
          if (subscribedTo == eventType || subscribedTo == DomainEvent.class) {
            subscriber.handleEvent(aDomainEvent);
          }
        }
      }
    } finally {
      publishing.set(Boolean.FALSE);
    }
  }

  public DomainEventPublisher reset() {
    if (!publishing.get()) {
      subscribers.set(null);
    }
    return this;
  }

  public void subscribe(DomainEventSubscriber<DomainEvent> aSubscriber) {
    if (publishing.get()) {
      return;
    }
    List<DomainEventSubscriber<DomainEvent>> registeredSubscribers = subscribers.get();
    if (registeredSubscribers == null) {
      registeredSubscribers = new ArrayList<DomainEventSubscriber<DomainEvent>>();
      subscribers.set(registeredSubscribers);
    }
    registeredSubscribers.add(aSubscriber);
  }
}