/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.axonbit.usermanager.domain.model.user;

import java.time.LocalDateTime;

import pl.axonbit.usermanager.domain.model.DomainEvent;

public final class UserRegistered implements DomainEvent {

    private final LocalDateTime occurredOn;
    private final String userId;
    private final String toAddress;

    public UserRegistered(LocalDateTime occurredOn, String userId, String toAddress) {
        this.occurredOn = occurredOn;
        this.userId = userId;
        this.toAddress = toAddress;
    }

    @Override
    public LocalDateTime occurredOn() {
        return this.occurredOn;
    }

    public String getUserId() {
        return userId;
    }

    public String getToAddress() {
        return toAddress;
    }

    @Override
    public String toString() {
        return "UserRegistered [occurredOn=" + occurredOn + ", toAddress=" + toAddress + ", userId=" + userId + "]";
    }

}
