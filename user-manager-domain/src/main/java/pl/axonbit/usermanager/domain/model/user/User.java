/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.axonbit.usermanager.domain.model.user;

import java.time.LocalDateTime;

import pl.axonbit.usermanager.domain.model.AggregateRoot;
import pl.axonbit.usermanager.domain.model.DomainEventPublisher;

public class User implements AggregateRoot {

    private UserId userId;
    private String password;
    private String login;
    private String email;
    private UserStatus userStatus;

    public User(final UserId userId, final String password, final String login, final String email,
            final UserStatus userStatus) {
        this.userId = userId;
        this.password = password;
        this.login = login;
        this.email = email;
        this.userStatus = userStatus;
    }

    public UserId getUserId() {
        return userId;
    }

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }

    public String getEmail() {
        return email;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public static User registerUser(final UserId userId, final String password, final String login,
            final String email) {

        return new User(userId, password, login, email, UserStatus.NEW);
    }

    public void registerUser() {
        DomainEventPublisher.instance()
                .publish(new UserRegistered(LocalDateTime.now(), this.userId.getId(), this.email));
    }

    @Override
    public String toString() {
        return "User [email=" + email + ", login=" + login + ", password=" + password + ", userId=" + userId
                + ", userStatus=" + userStatus + "]";
    }

}
