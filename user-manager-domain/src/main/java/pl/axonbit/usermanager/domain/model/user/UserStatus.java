package pl.axonbit.usermanager.domain.model.user;

public enum UserStatus {

    NEW, CONFIRMED, BANNED;

}
