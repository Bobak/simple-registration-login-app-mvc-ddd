/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.axonbit.usermanager.domain.model.user;

import pl.axonbit.usermanager.domain.model.Repository;

public interface UserRepository extends Repository {

    void insert(User user);

    User get(UserId userId);

    User getByLogin(String login);

    User getByLoginPasswordEncryped(String login, String encryptedPassword);

    UserId nextIdentity();

}
