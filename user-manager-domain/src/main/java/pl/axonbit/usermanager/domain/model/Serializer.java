package pl.axonbit.usermanager.domain.model;

public interface Serializer<T, T1> {

    public T1 deserialize(T object);
}
