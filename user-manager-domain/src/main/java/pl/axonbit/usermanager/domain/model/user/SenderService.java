package pl.axonbit.usermanager.domain.model.user;

import pl.axonbit.usermanager.domain.model.Service;

public interface SenderService extends Service {

    void sendEmail(Email email);
}
