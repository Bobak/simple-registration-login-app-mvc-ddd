package pl.axonbit.usermanager.domain.model.user;

import pl.axonbit.usermanager.domain.model.Service;

public class UserService implements Service {

    public void validatePassword(String password) {
        if (!(password.length() == 12)) {
            throw new IllegalStateException("The password has to be builded exact upon 12 char !");
        }
    }

    public void validateLogin(String login) {

        if (!(login.matches("^[0-9]*$") && login.length() == 7)) {
            throw new IllegalStateException("The login consists of numbers only and has to be exactly 7 numbers !");
        }
    }
}
