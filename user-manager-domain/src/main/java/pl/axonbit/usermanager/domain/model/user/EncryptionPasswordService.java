/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.axonbit.usermanager.domain.model.user;

import pl.axonbit.usermanager.domain.model.Service;

public interface EncryptionPasswordService extends Service {

    public String hashPassword(String password);

}
