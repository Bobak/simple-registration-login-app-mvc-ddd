package pl.axonbit.usermanager.application.user;

import javax.inject.Inject;

import pl.axonbit.usermanager.domain.model.Service;
import pl.axonbit.usermanager.domain.model.user.EncryptionPasswordService;
import pl.axonbit.usermanager.domain.model.user.User;
import pl.axonbit.usermanager.domain.model.user.UserRepository;
import pl.axonbit.usermanager.domain.model.user.UserService;

public class UserApplicationService implements Service {

    private final UserRepository userRepository;
    private final UserService userService;
    private final EncryptionPasswordService encryptionPasswordService;

    @Inject
    public UserApplicationService(UserRepository userRepository, UserService userService,
            EncryptionPasswordService encryptionPasswordService) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.encryptionPasswordService = encryptionPasswordService;
    }

    public void registerNewUser(RegisterUserCommand aCommand) {

        userService.validateLogin(aCommand.getLogin());
        userService.validatePassword(aCommand.getPassword());

        final var currentUser = userRepository.getByLogin(aCommand.getLogin());

        if (currentUser != null) {
            throw new IllegalStateException("User with defined login : " + aCommand.getLogin() + " exsist !");
        }

        final var password = encryptionPasswordService.hashPassword(aCommand.getPassword());
        final var userId = userRepository.nextIdentity();

        final var user = User.registerUser(userId, password, aCommand.getLogin(), aCommand.getEmail());
        userRepository.insert(user);
        user.registerUser();


    }

    

}
