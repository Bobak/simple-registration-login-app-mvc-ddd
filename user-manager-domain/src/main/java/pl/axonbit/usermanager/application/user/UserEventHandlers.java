package pl.axonbit.usermanager.application.user;

import javax.inject.Inject;

import pl.axonbit.usermanager.domain.model.Service;
import pl.axonbit.usermanager.domain.model.user.Email;
import pl.axonbit.usermanager.domain.model.user.SenderService;
import pl.axonbit.usermanager.domain.model.user.UserRegistered;

public class UserEventHandlers implements Service {

    private final SenderService senderService;

    @Inject
    public UserEventHandlers(SenderService senderService) {
        this.senderService = senderService;
    }

    public void when(final UserRegistered aDomainEvent) {

        if (aDomainEvent == null) {
            return;
        }

        var email = new Email();
        email.setFrom("some.email@domain.pl");
        email.setSubject("You have been registered into our system");
        email.setTo(aDomainEvent.getToAddress());
        email.setContent("Congratulation !!! You have been succesfully registered into our servcie !!!");

        senderService.sendEmail(email);

    }

    

}
