package pl.axonbit.usermanager.application.user;

import pl.axonbit.usermanager.domain.model.Command;

public class RegisterUserCommand implements Command {

    private final String password;
    private final String login;
    private final String email;

    public RegisterUserCommand(String password, String login, String email) {
        this.password = password;
        this.login = login;
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "RegisterUserCommand [email=" + email + ", login=" + login + ", password=" + password + "]";
    }

    

}
