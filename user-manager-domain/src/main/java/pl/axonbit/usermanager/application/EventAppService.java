/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.axonbit.usermanager.application;

import java.util.logging.Logger;

import pl.axonbit.usermanager.domain.model.DomainEvent;
import pl.axonbit.usermanager.domain.model.DomainEventPublisher;
import pl.axonbit.usermanager.domain.model.DomainEventSubscriber;

public class EventAppService{

    private static final Logger LOGGER = Logger.getLogger(EventAppService.class.getName());

    public EventAppService() {
        listen();
    }

    private static void listen() {
        DomainEventPublisher.instance().reset();

        DomainEventPublisher.instance().subscribe(new DomainEventSubscriber<DomainEvent>() {

            public void handleEvent(DomainEvent aDomainEvent) {
                LOGGER.info(aDomainEvent.toString());
            }

            public Class<DomainEvent> subscribedToEventType() {
                return DomainEvent.class;
            }
        });
    }
}
