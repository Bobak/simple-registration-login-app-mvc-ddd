
= SIMPLE REGISTRATION/LOGIN APP  =

---

NOTE: I recommend to open this document into browser outside Github/Gitlab - some of text editor can cause problem with proper displaying ASCII schemas!!

I want to mention that Domain Event bus is just only inmemory bus (for production purpose I recommend to create Domain Event bus on somthing else Kafka/RabbbitMQ or other technoligies which support guaranted delivery in order to achive real strong eventual consistency). 

We react on UserRegistered event and in our case we save Email to local disk (LocalFileSenderService) in order to sent email in real you just need to create new implmentation of SenderService.

---

= Domain Description =

Our user manager domain allow user to registry themself. The login consists of numbers only and has to be exactly 7 numbers, the password has to be builded exact upon 12 char, after succesfully registration user will recive email, data related to users have to be stored into one of the DB(mysql/postgresql).



===== Architecture of domain application =====

**In our system we gonna use hexagonal architecture (sometimes called port - adapter architecture).**
- PORT -> most often interface
- ADAPTER -> implementation of that interface

NOTE: The idea of Hexagonal architecture is to isolate central logic (bussines model) from outside of system, which cause that we gonna increase portability of our application and can quickly be adapted for new infrastructure usage without touching bussines model. 

 

```

                                     +-----------------------------------------------------------------------------------------+
                                     |                                                                                         |
                                     |              Hexagonal architecture (as square view) for domain application             |
                                     |                                                                                         |
                                     +-----------------------------------------------------------------------------------------+






                                     +-----------------------------------------------------------------------------------------+
                                     |                                                                                         |
                                     |                                      Adapters                                           |
                                     |                                                                                         |
                                     |        +----------------------------------------------------------------------+         |
                                     |        |                                                                      |     +-----------------------> MongoDB/SQL/etc..
                                     |        |                         Application layer                            |         |
                                     |        |                              (PORTS)                                 |         |
                                     |        |       +----------------------------------------------------+         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                    Domain Layer                    |         |         |
SOAP/REST/CLI/etc.. <--------------------+    |       |                                                    |         |     +-----------------------> HTTP/FTP/SMTP/etc...
                                     |        |       |                                                    |         |         |
                                     |        |       |                      (PORTS)                       |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       |                                                    |         |         |
                                     |        |       +----------------------------------------------------+         |         |
                                     |        |                                                                      |     +-----------------------> Kafka/SQS/etc...
                                     |        |                                                                      |         |
                                     |        +----------------------------------------------------------------------+         |
                                     |                                                                                         |
                                     |                                                                                         |
                                     |                                                                                         |
                                     +-----------------------------------------------------------------------------------------+

```

---

== Tactical Modeling ==

**Quick description:**
- Aggregate, Entity, Value Object (Entity, Value Object types must remain transactionally consistent throughout the Aggregate’s lifetime)
- Repository (Aggregate is persisted using repository and later searched for within and retrieved from it)
- Services (stateless operations to perform business logic that don’t fit naturally as an operation on an Entity or a Value Object)
 - Domain Service - carry out domain-specific operations, which may involve multiple domain objects
 - Application Service - carry out application-specific operations transactions and event handling (in case of CQRS also Commands can be defined here)
- Domain Events (to indicate the occurrence of significant happenings in the domain. Domain Events can be modeled on a few different ways. When they capture occurrences that are a result of some Aggregate command operation, the Aggregate itself publishes the Event) and can be published from domain servcie.


===== Modules of application =====

In order to incrase portability of our domain business application has been splitted into two modules:

1.Module Domain - is responsible for all important operation from domain point of view (validation/email sending/persistance). 
2.Module Web - is resposible for all web capabilities, in short we can say that web module is a client for our domain module. 

This opration open our doors in future for migration processes to differnt architectures microservices/serverless different login capabilities (OAuth/JWT etc..).











